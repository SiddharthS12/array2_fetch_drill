const fetchTodos = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/todos");
  return new Promise((resolve, reject) => {
    response
      .then((todosData) => {
        todosData
          .json()
          .then((todos) => {
            resolve(todos);
          })
          .catch((error) => {
            reject("Failed in TodosData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching todos failed");
      });
  });
};


const fetchUserById = (id) => {
  let response = fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`);
  return new Promise((resolve, reject) => {
    response
      .then((userData) => {
        userData
          .json()
          .then((userDetails) => {
            //   console.log(userDetails);
            resolve(userDetails);
          })
          .catch((error) => {
            // console.log(error);
            reject(error);
          });
      })
      .catch((error) => {
        // console.log("Response failed");
        reject(error);
      });
  });
};

fetchTodos()
  .then((todos) => {
    const firstTodo = todos[0];
    return firstTodo;
  })
  .then((firstTodo) => {
    let id = firstTodo.userId;
    return fetchUserById(id).then((userDetails) => {
      return userDetails;
    });
  })
  .then((user) => {
    console.log(user);
  })
  .catch((error) => console.log(error));
