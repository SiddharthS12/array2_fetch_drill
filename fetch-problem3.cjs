const fetchUsers = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/users");
  return new Promise((resolve, reject) => {
    response
      .then((usersData) => {
        usersData
          .json()
          .then((users) => {
            resolve(users);
          })
          .catch((error) => {
            reject("Failed in usersData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching users failed");
      });
  });
};

const fetchTodos = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/todos");
  return new Promise((resolve, reject) => {
    response
      .then((todosData) => {
        todosData
          .json()
          .then((todos) => {
            resolve(todos);
          })
          .catch((error) => {
            reject("Failed in TodosData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching todos failed");
      });
  });
};

fetchUsers()
  .then((users) => {
    console.log(users);
    return fetchTodos();
  })
  .catch((error) => {
    console.log(error);
  })
  .then((todos) => {
    console.log(todos);
  })
  .catch((error) => {
    console.log(error);
  });
