const fetchUsers = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/users");
  return new Promise((resolve, reject) => {
    response
      .then((usersData) => {
        usersData
          .json()
          .then((users) => {
            resolve(users);
          })
          .catch((error) => {
            reject("Failed in usersData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching users failed");
      });
  });
};

const fetchUserById = (id) => {
  let response = fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`);
  return new Promise((resolve, reject) => {
    response
      .then((userData) => {
        userData
          .json()
          .then((userDetails) => {
            //   console.log(userDetails);
            resolve(userDetails);
          })
          .catch((error) => {
            // console.log(error);
            reject(error);
          });
      })
      .catch((error) => {
        // console.log("Response failed");
        reject(error);
      });
  });
};

fetchUsers().then((users) => {
  const ans = users.map((user) => {
    let id = user.id;
    return fetchUserById(id).then((userDetails) => {
      return userDetails;
    });
  });
  Promise.all(ans).then((user) => {
    console.log(user);
  });
});
