const fetchUsers = () => {
    let response = fetch("https://jsonplaceholder.typicode.com/users");
    return new Promise((resolve, reject) => {
      response
        .then((usersData) => {
          usersData
            .json()
            .then((users) => {
              resolve(users);
            })
            .catch((error) => {
              reject("Failed in usersData");
            });
        })
        .catch((error) => {
          console.log(error);
          reject("Fetching users failed");
        });
    });
  };
  
  fetchUsers().then((users) => { console.log(users); })
  .catch(error=> console.log(error))