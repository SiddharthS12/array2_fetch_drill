/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended



*/
const fetchUsers = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/users");
  return new Promise((resolve, reject) => {
    response
      .then((usersData) => {
        usersData
          .json()
          .then((users) => {
            resolve(users);
          })
          .catch((error) => {
            reject("Failed in usersData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching users failed");
      });
  });
};

// fetchUsers().then((users) => { console.log(users); })
// .catch(error=> console.log(error))

const fetchTodos = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/todos");
  return new Promise((resolve, reject) => {
    response
      .then((todosData) => {
        todosData
          .json()
          .then((todos) => {
            resolve(todos);
          })
          .catch((error) => {
            reject("Failed in TodosData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching todos failed");
      });
  });
};

// fetchTodos().then((todos) => {
//     console.log(todos);
// }).catch ((error) => {
//     console.log(error);
// })

// fetchUsers()
//   .then((users) => {
//     console.log(users);
//     return fetchTodos();
//   })
//   .catch((error) => {
//     console.log(error);
//   })
//   .then((todos) => {
//     console.log(todos);
//   })
//   .catch((error) => {
//     console.log(error);
//   });

const fetchUserById = (id) => {
  let response = fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`);
  return new Promise((resolve, reject) => {
    response
      .then((userData) => {
        userData
          .json()
          .then((userDetails) => {
            //   console.log(userDetails);
            resolve(userDetails);
          })
          .catch((error) => {
            // console.log(error);
            reject(error);
          });
      })
      .catch((error) => {
        // console.log("Response failed");
        reject(error);
      });
  });
};

// fetchUserById(1).then((userData) => {console.log(userData); })

// fetchUsers().then((users) => {
//   const ans = users.map((user) => {
//     let id = user.id;
//     return fetchUserById(id).then((userDetails) => {
//       return userDetails;
//     });
//   });
//   Promise.all(ans).then((user) => {
//     console.log(user);
//   })
// });

fetchTodos()
  .then((todos) => {
    const firstTodo = todos[0];
    return firstTodo;
  })
  .then((firstTodo) => {
    let id = firstTodo.userId;
    return fetchUserById(id).then((userDetails) => {
      return userDetails;
    });
  })
  .then((user) => {
    console.log(user);
  })
  .catch((error) => console.log(error));
