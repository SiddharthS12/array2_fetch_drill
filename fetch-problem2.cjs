const fetchTodos = () => {
  let response = fetch("https://jsonplaceholder.typicode.com/todos");
  return new Promise((resolve, reject) => {
    response
      .then((todosData) => {
        todosData
          .json()
          .then((todos) => {
            resolve(todos);
          })
          .catch((error) => {
            reject("Failed in TodosData");
          });
      })
      .catch((error) => {
        console.log(error);
        reject("Fetching todos failed");
      });
  });
};

fetchTodos()
  .then((todos) => {
    console.log(todos);
  })
  .catch((error) => {
    console.log(error);
  });
